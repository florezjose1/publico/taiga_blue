# Theme blue taiga

Version Taiga compiled: [4.2.13](https://github.com/taigaio/taiga-front/commits/4.2.13)


1. Clone resporitory [https://github.com/taigaio/taiga-front.git](https://github.com/taigaio/taiga-front.git)

2. `$ npm install` or `$ yarn install`
    > *NOTE: Make sure all dependencies have been installed.*

3. In folder `app/themes/` clone this repository.

4. Make the following changes.

   4.1. Menu: Copy images of folder `app/themes/taiga_blue/images/menu/` in folder `app/images/`.
   
   4.2. Logo Small left top: Replace file of folder `app/svg/logo.svg` with image of `app/themes/taiga_blue/images/logo/logo.svg`.
   
   4.3. Logo sesión, register, forgot password: Replace file of folder `app/svg/logo-color.svg` with image of `app/themes/taiga_blue/images/logo/logo-color.svg`.
   
   4.4. spinner: Replace file of `app/svg/spinner.svg` with image of `app/themes/taiga_blue/images/logo/spinner.svg`.
   
   4.5. Background image page: Copy images of `app/themes/taiga_blue/images/background/` in `app/images/`.


5. Optional.
   5.1. Change placeholder input password: Search changed text of file `app/locales/taiga/locale-es.json:327` and `app/locales/taiga/locale-es.json:343` by a text apropied.

6. Compile app: `$ gulp deploy`

7. Copy and paste files of `disk` in `/home/taiga/taiga-front-dist/dist` server.

8. Added name template in settings, edit file `config.json`. `"themes": [... , "taiga_blue"],`
   
   8.1. Optional: Default theme: `"defaultTheme": "taiga_blue",`


